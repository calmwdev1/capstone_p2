export const BASE_URL = 'https://648c5f328620b8bae7ecca2e.mockapi.io/product';

export let phoneService = {
    update(id, data) {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: 'PUT',
            data: data,
        })
    }
}