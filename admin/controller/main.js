import { BASE_URL, phoneService } from "../services/services.js";
import {
  checkForm,
  getDataForm,
  renderProductList,
  resetFromInput,
  showSuccess,
  showThongTinLenFom,
  sortPhoneHighToLow,
  sortPhoneLowToHigh,
} from "./controller.js";

let idPhone = null;

let fetchProductList = () => {
  axios
    .get(`${BASE_URL}`)
    .then((res) => {
      renderProductList(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

fetchProductList();

window.creatProduct = () => {
  let newProduct = getDataForm();
  let checked = checkForm(newProduct);

  if (checked) {
    axios
      .post(BASE_URL, newProduct)
      .then((res) => {
        showSuccess();
        // reset tất cả ô inupt
        resetFromInput();
        $("#myModal").modal("hide");
        fetchProductList();
      })
      .catch((err) => {
        console.log(err);
      });
  }
};

let deletePhone = (id) => {
  axios
    .delete(`${BASE_URL}/${id}`)
    .then((res) => {
      showSuccess("Xóa điện thoại thành công");
      fetchProductList();
    })
    .catch((err) => {
      console.log(err);
    });
};

window.deletePhone = deletePhone;

let modifyPhone = (id) => {
  idPhone = id;

  axios
    .get(`${BASE_URL}/${id}`)
    .then((res) => {
      $("#myModal").modal("show");
      showThongTinLenFom(res.data);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.modifyPhone = modifyPhone;

let updatePhone = () => {
  var data = getDataForm();

  phoneService
    .update(idPhone, data)
    .then((res) => {
      showSuccess("Cập nhật thành công");
      $("#myModal").modal("hide");
      resetFromInput();
      fetchProductList();
    })
    .catch((err) => {
      console.log(err);
    });
};

window.updatePhone = updatePhone;

let searchPhone = () => {
  axios
    .get(BASE_URL)
    .then((res) => {
      let data = document.getElementById("search").value;
      let listPhone = [];
      res.data.forEach((item) => {
        if (item.name == data) {
          listPhone.push(item);
        }
      });
      renderProductList(listPhone);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.searchPhone = searchPhone;

let arrangePhoneLowToHigh = () => {
  axios
    .get(`${BASE_URL}`)
    .then((res) => {
      let phones = res.data;
      let sortedListPhone = sortPhoneLowToHigh(phones);
      renderProductList(sortedListPhone);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.arrangePhoneLowToHigh = arrangePhoneLowToHigh;

let arrangePhoneHighToLow = () => {
  axios
    .get(`${BASE_URL}`)
    .then((res) => {
      let phones = res.data;
      let sortedListPhone = sortPhoneHighToLow(phones);
      renderProductList(sortedListPhone);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.arrangePhoneHighToLow = arrangePhoneHighToLow;

window.resetFromInput = resetFromInput;
