import { checkLink, checkNumber, checkType, noEmpty } from "./validate.js";

export let renderProductList = (phoneArr) => {
  let contentHTML = "";
  phoneArr.forEach((item) => {
    let { id, name, price, screen, backCamera, frontCamera, img, desc, type } =
      item;
    let contentTr = `<tr>
            <td>${id}</td>
            <td>${name}</td>
            <td>$${price}</td>
            <td>
                <img style="width: 30%" src="${img}" alt="hình ảnh" />
            </td>
            <td>${desc}</td>
            <td class="d-flex">
                <button onclick="deletePhone(${id})" class="btn btn-danger mr-1">Xóa</button>
                <button onclick="modifyPhone(${id})" class="btn btn-warning ml-1">Sửa</button>
            </td>
        </tr>`;
    contentHTML += contentTr;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

// sweet alert
export let showSuccess = (title = "Thành công") => {
  Swal.fire({
    position: "center-center",
    icon: "success",
    title,
    showConfirmButton: false,
    timer: 1000,
    timerProgressBar: true,
  });
};

export let getDataForm = () => {
  let name = document.getElementById("TenSP").value;
  let price = document.getElementById("GiaSP").value;
  let screen = document.getElementById("ScreenSP").value;
  let backCamera = document.getElementById("BackSP").value;
  let frontCamera = document.getElementById("FrontSP").value;
  let img = document.getElementById("ImgSP").value;
  let desc = document.getElementById("DescSP").value;
  let type = document.getElementById("TypeSP").value;
  return {
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc,
    type,
  };
};

export let showThongTinLenFom = (dataPhone) => {
  document.getElementById("TenSP").value = dataPhone.name;
  document.getElementById("GiaSP").value = dataPhone.price;
  document.getElementById("ScreenSP").value = dataPhone.screen;
  document.getElementById("BackSP").value = dataPhone.backCamera;
  document.getElementById("FrontSP").value = dataPhone.frontCamera;
  document.getElementById("ImgSP").value = dataPhone.img;
  document.getElementById("DescSP").value = dataPhone.desc;
  document.getElementById("TypeSP").value = dataPhone.type;
};

export let sortPhoneLowToHigh = (phones) => {
  phones.sort((a, b) => a.price - b.price);
  return phones;
};

export let sortPhoneHighToLow = (phones) => {
  phones.sort((a, b) => b.price - a.price);
  return phones;
};

export let onLoading = () => {
  document.getElementById("spinner-admin").style.display = "flex";
};

export let offLoading = () => {
  document.getElementById("spinner-admin").style.display = "none";
};

export let checkForm = (data) => {
  let isValid =
    noEmpty("span-price", "giá tiền không được để trống", data.price) &&
    checkNumber(data.price);
  isValid &= noEmpty("span-name", "tên không được để trống", data.name);
  isValid &= noEmpty("span-screen", "screen không được để trống", data.screen);
  isValid &= noEmpty(
    "span-back-camera",
    "back camera không được để trống",
    data.backCamera
  );
  isValid &= noEmpty(
    "span-front-camera",
    "front camera không được để trống",
    data.backCamera
  );
  isValid &= noEmpty(
    "span-desc",
    "description không được để trống",
    data.backCamera
  );
  isValid &= checkLink(data.img);
  isValid &= checkType(data.type);
  return isValid;
};

export let resetFromInput = () => {
  document.getElementById("form-body").reset();
};
