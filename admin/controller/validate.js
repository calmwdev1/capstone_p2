function showMessage(idSpan, message) {
    document.getElementById(idSpan).innerText = message;
} 

export let checkNumber = (value) => {
    const reg = new RegExp('^[0-9]+$');
    if(reg.test(value)) {
        showMessage('span-price', '');
        return true;
    } else {
        showMessage('span-price', 'giá tiền chỉ bao gồm số !');
        return false;
    };
}

export let checkLink = (value) => {
    var valid = /^(ftp|http|https):\/\/[^ "]+$/.test(value);
    if(valid) {
        showMessage('span-img', '');
        return true;
    } else {
        showMessage('span-img', 'điền url hình ảnh !');
        return false;
    }
}

export let checkType = (value) => {
    if(value == '') {
        showMessage('span-type', 'type không được để trống');
        return false;
    } else {
        showMessage('span-type', '');
        return true;
    }
}

export let noEmpty = (idSpan, message, value) => {
    if(value.length == 0) {
        showMessage(idSpan, message);
        return false;
    } else {
        showMessage(idSpan, '');
        return true;
    };
}