function renderProductList(productArr) {
  var contentHTML = "";
  productArr.forEach(function (item) {
    var content = `<div class="item">
        <div class="img_wraper">
            <img src="${item.img}" alt="">
        </div>
        <div class="desc flex-col items-center justify-center space-x-5">
            <div class="title w-full  ">
        <div class="name ">
            <h3 class=" text-center">${item.name}</h3>
        </div>
            </div>
            <div class="price -space-x-8">
                <h3>${item.price}</h3>
                <button onclick = "addCart(${item.id})" class="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded">Add to cart</button>
            </div>
        </div>
    </div>`;
    contentHTML += content;
    // console.log("item",item)
    // console.log("itemrd",item.id)
  });

  document.getElementById("list").innerHTML = contentHTML;
}
function renderCart(cartArr) {
  var contentHTML = "";
  cartArr.forEach(function (item) {
    var content = `<li class="flex py-6">
                            <div class="h-24 w-24 flex-shrink-0 overflow-hidden rounded-md border border-gray-200">
                              <img src="${item.img}" alt="Salmon orange fabric pouch with match zipper, gray zipper pull, and adjustable hip belt." class="h-full w-full object-cover object-center">
                            </div>

                            <div class="ml-4 flex flex-1 flex-col">
                              <div>
                                <div class="flex justify-between text-base font-medium text-gray-900">
                                  <h3>
                                    <a href="#">${item.name}</a>
                                  </h3>
                                  <p class="ml-4" id="cartMoney">${item.price}</p>
                                </div>
                                <p class="mt-1 text-sm text-gray-500">${item.type}</p>
                              </div>
                              <div class="flex flex-1 items-end justify-between text-sm">
                                <p class="text-gray-500">${item.desc}</p>

                                <div class="flex">
                                  <button onclick="spliceCart(${item.id})" type="button" class="font-medium text-indigo-600 hover:text-indigo-500">
                                    Remove
                                  </button>
                                </div>
                                <div>
                                <div class="w-auto h-auto">
                                <p id="countNumber"> 1 </p>
                                <div class="flex-1 h-full">
                                
                                  <div class="flex items-center justify-center flex-1  border border-gray-400 rounded-full">
                                    <div class="relative cursor-pointer" onclick = "inNumber()">
                                      
                                      <svg xmlns="http://www.w3.org/2000/svg" class="h-4 w-4 text-gray-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                      </svg>
                                    </div>
                                  </div>
                                </div>
                              </div>
                                 </div>
                              </div>
                            </div>
                          </li>`;
    contentHTML += content;
  });
  document.getElementById("cartlist").innerHTML = contentHTML;
}

function batLoading() {
  document.getElementById("spinner").style.display = "flex";
}

function tatLoading() {
  document.getElementById("spinner").style.display = "none";
}


