const  BASE_URL= "https://648c5f328620b8bae7ecca2e.mockapi.io/product";


var productServ={
    // goi api
    getList: () =>{
    return       axios({
            url:BASE_URL,
            method:"GET",
        })
    },
    // xoa
    delete: (id) => {
        return axios({
            url:`${BASE_URL}/${id}`,
            method: "DELETE",
            
        });
    },

    // them
     create: (product) => {
     return    axios({
            url: BASE_URL,
            method:"POST",
            data: product,
        });
    },
    getById: function(id){
        return axios({
            url:`${BASE_URL}/${id}`,
            method:"GET",
        });
    },
    update: function(id,product){
        return axios({
            url:`${BASE_URL}/${id}`,
            method:"PUT",
            data:product,
        });
    },
};

